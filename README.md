# orgchart

orgchart is a python package for retrieving organizational charts.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install orgchart.

Unix/macOS:
```bash
python3 -m pip install --index-url https://test.pypi.org/simple/ organization-chart
```
Windows:
```bash
py -m pip install --index-url https://test.pypi.org/simple/ organization-chart
```

## Usage

```python
from orgchart import OrgChart

orgchart = OrgChart("organizations.json", "employees.json")

# returns organization list
orgchart.organization.list()

# gets an organization by id
app_dev = orgchart.organization.get(5)

# returns the supervisor of organization app_dev
app_dev.supervisor

# returns the employees of organization app_dev
app_dev.employees

# gets an employee by email
thud = orgchart.employee.get("thud@mail")

# returns the supervisor of employee thud
thud.supervisor

# returns the organizations employee thud belongs to
thud.organizations

```

## License
[MIT](LICENSE)
