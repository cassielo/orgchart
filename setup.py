from setuptools import setup, find_packages

setup(
    name='organization-chart',
    version='0.0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='A python package for retrieving organizational charts',
    long_description_content_type="text/markdown",
    long_description=open('README.md').read(),
    url='https://gitlab.com/cassielo/orgchart',
    python_requires='>=3.6',
    author='Cassie Lo',
    author_email='musicivy@gmail.com'
)
