import pytest
from orgchart import OrgChart

@pytest.fixture
def org():
	return OrgChart("data/organizations.json", "data/employees.json")

def test_fileError(capsys):
	orgc = OrgChart("data/organizations.json", "data/e.json")
	captured = capsys.readouterr()
	assert captured.out == "[Errno 2] No such file or directory: 'data/e.json'\n"

def test_fileFormatError(capsys):
	orgc = OrgChart("data/employees.json", "data/organizations.json")
	captured = capsys.readouterr()
	assert captured.out == "[Error] Wrong json format\n"

def test_get(org):
	assert org.organization.get(5).id == 5

def test_getError(org, capsys):
	assert org.organization.get(100) == None
	captured = capsys.readouterr()
	assert captured.out == "[Error] Can't find 100\n"

@pytest.fixture
def org5(org):
	return org.organization.get(5)

@pytest.fixture
def empThud(org):
	return org.employee.get("thud@mail")

def test_organizationList(org):
	assert org.organization.list() == "[<Organization(id=1): President Office>, <Organization(id=2): Business Development Dept.>, <Organization(id=3): Product Design Dept.>, <Organization(id=4): Product Development Div.>, <Organization(id=5): Application Development Dept.>]"

def test_employeeList(org):
	assert org.employee.list() == "[<Employee: Bob>, <Employee: Baz>, <Employee: Foo>, <Employee: Qux>, <Employee: Grault>, <Employee: Thud>, <Employee: Corge>, <Employee: Fred>, <Employee: Amy>]"

def test_organizationAttribute(org,org5):
	assert org5.id == 5
	assert org5.parent_id == 4
	assert org5.organization_code == "app-dev"
	assert org5.organization_name == "Application Development Dept."
	assert org5.supervisor_id == "0101004"
	assert org5.is_active == True
	assert org5.supervisor == org.employee.get("qux@mail")
	assert org5.employees.list() == "[<Employee: Corge>, <Employee: Fred>]"
	assert org5.parent_organization == org.organization.get(4)

def test_employeeAttribute(org,empThud):
	assert empThud.id == 6
	assert empThud.employee_no == "0101006"
	assert empThud.employee_name == "Thud"
	assert empThud.email == "thud@mail"
	assert empThud.onboarding_date == "20200201"
	assert empThud.organization_id == "product-design"
	assert empThud.job_title == "Designer"
	assert empThud.gender == "M"
	assert empThud.supervisor == org.employee.get("foo@mail")
	assert empThud.organizations.list() == "[<Organization(id=3): Product Design Dept.>]"

	