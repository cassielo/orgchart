import json
from os import path

class Organization:

	def __init__(self, id, parentId, organizationCode, organizationName, supervisorId, isActive):

		self.id = id
		self.parent_id = parentId
		self.organization_code = organizationCode
		self.organization_name = organizationName
		self.supervisor_id = supervisorId
		self.is_active = isActive
		self.supervisor = None
		self.employees = Map()
		self.parent_organization = None

	def __repr__(self):
		return "<Organization(id=" + str(self.id) + "): " + self.organization_name + ">"

class Employee:

	def __init__(self, id, employeeNo, employeeName, email, onboardingDate, organizationId, jobTitle, Gender):

		self.id = id
		self.employee_no = employeeNo
		self.employee_name = employeeName
		self.email = email
		self.onboarding_date = onboardingDate
		self.organization_id = organizationId
		self.job_title = jobTitle
		self.gender = Gender
		self.supervisor = None
		self.organizations = Map()

	def __repr__(self):
		return "<Employee: " + self.employee_name + ">"

class Map:

	def __init__(self):
		self.objects = {}
	def get(self, key):
		if key in self.objects.keys():
			return self.objects[key]
		else:
			print("[Error] Can't find " + str(key))
			return None
	def list(self):
		return str(list(self.objects.values()))
	def __repr__(self):
		return str(list(self.objects.values()))

class OrgChart:

	def __init__(self, organizationFile, employeeFile):

		self.organization = Map()
		self.employee = Map()
		self.readFile(organizationFile, employeeFile)

	def readFile(self, organizationFile, employeeFile):

		try:
			# Read files
			with open(organizationFile) as o:
				organizationData = json.load(o)
			with open(employeeFile) as e:
				employeeData = json.load(e)

			# Create objects
			for org in organizationData:
				newOrganization = Organization(org['id'], org['parent_id'], org['organization_code'], org['organization_name'], org['supervisor_id'], org['is_active'])
				self.organization.objects[org['id']] = newOrganization
			for emp in employeeData:
				newEmployee = Employee(emp['id'], emp['employee_no'], emp['employee_name'], emp['email'], emp['onboarding_date'], emp['organization_id'], emp['job_title'], emp['gender'])
				self.employee.objects[emp['email']] = newEmployee

		except FileNotFoundError as f:
			print(f)
			return

		except BaseException:
			print("[Error] Wrong json format")
			return

		# Connect attributes
		for o in self.organization.objects.values():
			for e in self.employee.objects.values():
				if o.supervisor_id == e.employee_no:
					e.organizations.objects[o.id] = o
					o.supervisor = e
					break
			for p in self.organization.objects.values():
				if o.parent_id == p.id:
					o.parent_organization = p
					break

		for e in self.employee.objects.values():
			for o in self.organization.objects.values():
				if e.organization_id == o.organization_code:
					if e != o.supervisor:
						o.employees.objects[e.email] = e
						e.supervisor = o.supervisor
					e.organizations.objects[o.id] = o
					break
